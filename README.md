## Multi-armed bandits application

Work in progress.
This is an application of MAB which uses the following algorithms: EXP3, EXP3_S and UCB, it also applies generators, NumPy and Pandas.

## Deployment

1. Clone project.
2. Create and activate a virtual environment.
2. Install the dependencies.
3. Run the corresponding python project.
