"""
Agent's utility with and without energy flow offer
"""
import collections
import numpy as np


def find_energy_allocation(energy_profile, battery, time_slots, iscooperative):
    # Variables: energy_allocation, charge_discharge, waste, battery_status
    energy_allocation = np.zeros(time_slots)
    charge_discharge = np.zeros(time_slots)
    waste = np.zeros(time_slots)
    battery_status = np.zeros(time_slots)
    battery_loss = (1 - battery.efficiency) / time_slots

    if iscooperative:
        return waste
    else:
        first = True
        for timeslot in range(time_slots-1):
            if first:
                battery_status[timeslot] = 1 / battery.efficiency * battery.initial_status # initial battery status is a residual energy level or an amount of energy at the beginning of the period
                first = False

            if energy_profile.energy_harvesting[timeslot] >= energy_profile.load[timeslot]:
                energy_allocation[timeslot] = energy_profile.load[timeslot]
                if energy_profile.energy_harvesting[timeslot] - energy_profile.load[timeslot] > 1/battery.efficiency*(battery.max_capacity - battery_status[timeslot]):
                    charge_discharge[timeslot] = 1/battery.efficiency*(battery.max_capacity - battery_status[timeslot]) #charge
                    waste[timeslot] = energy_profile.energy_harvesting[timeslot] - energy_profile.load[timeslot] - charge_discharge[timeslot]
                else:
                   charge_discharge[timeslot] = energy_profile.energy_harvesting[timeslot] - energy_profile.load[timeslot]
            else:
                if energy_profile.load[timeslot] > energy_profile.energy_harvesting[timeslot] + battery_status[timeslot]:
                    energy_allocation[timeslot] = energy_profile.energy_harvesting[timeslot] + battery_status[timeslot]
                    charge_discharge[timeslot] = battery_status[timeslot] * -1 #discharge
                else:
                    energy_allocation[timeslot] = energy_profile.load[timeslot]
                    charge_discharge[timeslot] = (energy_profile.load[timeslot] - energy_profile.energy_harvesting[timeslot]) * -1

            battery_status[timeslot + 1] = battery_status[timeslot] + charge_discharge[timeslot]

        # For the last time slot and battery cycle:
        if energy_profile.energy_harvesting[time_slots-1] >= energy_profile.load[time_slots-1]:
            energy_allocation[time_slots-1] = energy_profile.load[time_slots-1]
            if energy_profile.energy_harvesting[time_slots-1] - energy_profile.load[time_slots-1] > battery_status[0]:
                charge_discharge[time_slots-1] = battery_status[0]
                waste[time_slots-1] = energy_profile.energy_harvesting[time_slots-1] - energy_profile.load[time_slots-1] - charge_discharge[time_slots-1]
            else:
                charge_discharge[time_slots-1] = energy_profile.energy_harvesting[time_slots-1] - energy_profile.load[time_slots-1]
        else:
            if energy_profile.load[time_slots-1] > energy_profile.energy_harvesting[time_slots-1] + battery_status[time_slots-1]:
                energy_allocation[time_slots-1] = energy_profile.energy_harvesting[time_slots-1] + battery_status[time_slots-1]
                charge_discharge[time_slots-1] = battery_status[0] * -1  #discharge
            else:
                energy_allocation[time_slots-1] = energy_profile.load[time_slots-1]
                charge_discharge[time_slots-1] = (energy_profile.load[time_slots-1] - energy_profile.energy_harvesting[time_slots-1]) * -1

        return energy_allocation, charge_discharge, waste, battery_status


if __name__ == "__main__":
    EnergyProfile = collections.namedtuple('EnergyProfile', [
        'energy_harvesting',
        'load'
    ])
    Battery = collections.namedtuple('Battery', [
        'initial_status',
        'max_capacity',
        'efficiency'
    ])
    energy_profile = EnergyProfile(energy_harvesting=(0, 9, 9), load=(2, 2, 2))
    battery = Battery(initial_status=1, max_capacity=708, efficiency=0.7)
    time_slots = 3
    iscooperative = False
    ealloc, c_d, waste, battery_status = find_energy_allocation(energy_profile, battery, time_slots, iscooperative)
