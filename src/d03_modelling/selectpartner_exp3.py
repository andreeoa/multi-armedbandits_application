"""
EXP3 algorithm is an extension of the Hedge algorithm for the adversarial MAB problem with partial information, that is,
 only the reward of the selected action can be observed.
"""


import math
import random


# Computing probability of every opponent
def compute_probability_distribution(weights, gamma):
    return tuple((1.0 - gamma) * (w / float(sum(weights))) + (gamma / len(weights)) for w in weights)


def draw_opponent_from_distribution(distribution):
    idx = 0
    interval_s = 0
    interval_e = 0

    # Getting normalised weights
    normalised_weights = [p / sum(distribution) for p in distribution]
    r = random.uniform(0, sum(normalised_weights))

    for p in normalised_weights:
        interval_e = interval_e + p
        if interval_s < r <= interval_e:
            return idx

        idx += 1
        interval_s = interval_e


def exp3(n_opponents, gamma, n_simulations):
    weights = [1.0] * n_opponents

    for simulation in range(n_simulations):
        distribution = compute_probability_distribution(weights, gamma)
        partner_selected = draw_opponent_from_distribution(distribution)
        reward = 1 # REWARD de la negociacion
        estimated_reward = reward / distribution[partner_selected]
        weights[partner_selected] *= math.exp(estimated_reward * gamma / n_opponents)

        yield partner_selected, reward, weights, distribution


# Learning the best opponent in n trials.
def learning_simulation():
    n_opponents = 3
    gamma = 0.2 # exploration parameter
    epochs = 2
    n_simulations = 100
    cumulative_reward = 0
    sim_no = 1;

    for epoch in range(epochs):
	# TODO
        # Change opponent options here
        for (partner_selected, r, w, p) in exp3(n_opponents, gamma, n_simulations):
            cumulative_reward += r

            print("iteration: " + str(sim_no))
            sim_no += 1


if __name__ == "__main__":
    learning_simulation()
