def print_message():
    for number in range(1, 101):
        multiple_3 = 0
        multiple_5 = 0

        if number % 3 == 0:
            multiple_3 = 1
        if number % 5 == 0:
            multiple_5 = 1
        if multiple_3 and multiple_5:
            print("FizzBuzz, the number was: " + str(number))
        else:
            if multiple_3 and multiple_5 != 1:
                print("Fizz, the number was: " + str(number))
            else:
                if multiple_3 != 1 and multiple_5:
                    print("Buzz, the number was: " + str(number))
                else:
                    print("The number is: " + str(number))


if __name__ == "__main__":
    print_message()
